﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cache_test
{
    internal class Write_through
    {
        private readonly Dictionary<string, object> cache = new Dictionary<string, object>();
        private readonly IDbConnection _dbConnection;

        public Write_through(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public T Get<T>(string key)
        {
            if (cache.TryGetValue(key, out object value))
            {
                return (T)value;
            }

            // Если значение не найдено в кэше, то загружаем его из бд
            var query = $"SELECT * FROM MyTable WHERE Key = '{key}'";
            var result = _dbConnection.QueryFirstOrDefault<T>(query);

            // И добавляем его в кэш и базу данных для последующих запросов
            if (result != default)
            {
                cache[key] = result;
                var insertQuery = $"INSERT INTO MyTable (Key, Value) VALUES ('{key}', '{result}')";
                _dbConnection.Execute(insertQuery);
            }

            return result;
        }

        public void Set<T>(string key, T value)
        {
            // Обновляем значение в кэше
            cache[key] = value;

            // Обновляем значение в бд
            var query = $"UPDATE MyTable SET Value = '{value}' WHERE Key = '{key}' ";
            _dbConnection.Execute(query);
        }
    }
}
