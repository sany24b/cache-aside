﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cache_test
{
    internal class Write_behind
    {
        private readonly Dictionary<string, object> cache = new Dictionary<string, object>();
        private readonly IDbConnection _dbConnection;
        private readonly BlockingCollection<KeyValuePair<string, object>> queue = new BlockingCollection<KeyValuePair<string, object>>();

        public Write_behind(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;

            // Запускаем отдельный поток для обновления кэша
            Task.Run(() =>
            {
                foreach (var item in queue.GetConsumingEnumerable())
                {
                    // Обновляем значение в кэше
                    cache[item.Key] = item.Value;

                    // Обновляем значение в базе данных
                    var query = $"UPDATE MyTable SET Value = '{item.Value}' WHERE Key = '{item.Key}'";
                    _dbConnection.Execute(query);
                }
            });
        }

        public T Get<T>(string key)
        {
            if (cache.TryGetValue(key, out object value))
            {
                return (T)value;
            }

            // Если значение не найдено в кэше, то загружаем его из бд
            var query = $"SELECT * FROM MyTable WHERE Key = '{key}'";
            var result = _dbConnection.QueryFirstOrDefault<T>(query);

            // И добавляем его в кэш для последующих запросов
            if (result != default)
            {
                cache[key] = result;

                // Добавляем обновление в очередь для асинхронного обновления кэша
                queue.Add(new KeyValuePair<string, object>(key, result));
            }

            return result;
        }

        public void Set<T>(string key, T value)
        {
            // Обновляем значение в кэше
            cache[key] = value;

            // Добавляем обновление в очередь для асинхронного обновления кэша
            queue.Add(new KeyValuePair<string, object>(key, value));
        }
    }
}
