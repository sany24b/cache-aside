﻿using System.Data;

namespace cache_test;
public class UsageExample
{
    private readonly Write_through _cache;
    public UsageExample(IDbConnection dbConnection) { _cache = new Write_through(dbConnection); }

    public int GetResult(int input)
    {
        var cacheKey = $"Result_{input}";

        // Проверяем, есть ли результат в кэше
        var result = _cache.Get<int>(cacheKey);

        if (result == default)
        {
            // Если результат не найден в кэше, то выполняем дорогостоящую операцию
            result = ExpensiveOperation(input);

            // И добавляем результат в кэш
            _cache.Set(cacheKey, result);
        }

        return result;
    }

    private int ExpensiveOperation(int input)
    {

        Thread.Sleep(2000);

        int result = 1;

        for (int i = 2; i <= input; i++)
        {
            result *= i;
        }

        return result;
    }
}